<?php
$connexion = mysqli_connect("bts.bts-malraux72.net:63330", "xxxUSERxxx", "xxxPASSWORDxxx", "xxxBASExxx");
if(mysqli_connect_errno($connexion)){
    die('Connexion impossible : ' . mysqli_connect_error());
}
else{
    $resultat = mysqli_query("SELECT identifiant, nom, prenom, pointure FROM ma_table_personne;");
    if($resultat != false){
        $nombre_tuples = mysqli_num_rows($resultat);
        print("La requête a retournée " . $nombre_tuples . " tuple");
        print($nombre_tuples>1 ? 's':'');
        print("\n");

        $numero_tuple = 1;
        while($tuple = mysqli_fetch_assoc($resultat)){
            print("Le tuple numéro $numero_tuple d'identifiant $tuple['identifiant'] a pour pointure $tuple['pointure'] et se nomme $tuple['prenom'] $tuple['nom']\n");
            $numero_tuple++;
        }
        if(is_resource($resultat)){
            mysqli_free_result($resultat);
        }
    }
    mysqli_close($connexion);
}
?>
